package com.tsadev.pricecheckbytsa;

public class Source {
    private String name;
    private String url;

    public Source() {
    }

    public Source(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public String getName() {
        return this.name;
    }

    public String getUrl() {
        return this.url;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setUrl(String url) {
        this.url = url;
    }
}
