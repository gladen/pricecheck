package com.tsadev.pricecheckbytsa;

import android.content.SearchRecentSuggestionsProvider;

public class RecentQueriesProvider extends SearchRecentSuggestionsProvider {
    public final static String AUTHORITY = "com.tsadev.pricecheckbytsa.RecentQueriesProvider";
    public final static int MODE = DATABASE_MODE_QUERIES;

    public RecentQueriesProvider() {
        setupSuggestions(AUTHORITY, MODE);
    }
}