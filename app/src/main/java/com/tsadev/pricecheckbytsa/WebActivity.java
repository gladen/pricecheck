package com.tsadev.pricecheckbytsa;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

@SuppressLint("all")
public class WebActivity extends Activity {
	
	WebView web;
	static final int RELOAD = 556632;
	static final int BACK = 133737;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_PROGRESS);
		getActionBar().setTitle("Search for " + "\"" + MainActivity.g + "\"");
		getActionBar().setIcon(android.R.color.transparent);
		setContentView(R.layout.web_layout);
		initWeb();
	}
	
	private void initWeb() {
		web = (WebView)findViewById(R.id.web);
		String url = getIntent().getExtras().getString("url");
		web.getSettings().setJavaScriptEnabled(true);
		web.getSettings().setBuiltInZoomControls(true);

		final ProgressBar pbar = (ProgressBar)findViewById(R.id.pbar);
		 web.setWebChromeClient(new WebChromeClient() {
			 public void onProgressChanged(WebView view, int progress) {
				 pbar.setVisibility(View.VISIBLE);
				 pbar.setProgress(progress * 1000);
				 if (progress == 100) pbar.setVisibility(View.GONE);
			 }

		 });
		 web.setWebViewClient(new WebViewClient() {
			 public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

			 }
		 });
		url += MainActivity.g;
		web.loadUrl(url);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//MenuItem back = menu.add(Menu.NONE, BACK, Menu.NONE, "Back");
		//back.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		//back.setIcon(android.R.drawable.ic_menu_revert);
		MenuItem f5 = menu.add(Menu.NONE,RELOAD,Menu.NONE,"Reload");
		f5.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		f5.setIcon(android.R.drawable.ic_popup_sync);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    case BACK:
	    	if(web.canGoBack()) web.goBack();
			else {
				web.getSettings().setBuiltInZoomControls(false);
				super.onBackPressed();
			}
	    	return true;
	    	
	    case RELOAD:
	    	web.reload();
	    	return true;
	    	
	    default:
	    	return super.onOptionsItemSelected(item);
	    }
	}
	@Override
	public void onBackPressed() {
		if (web.canGoBack()) web.goBack();
		else {
			web.getSettings().setBuiltInZoomControls(false);
			super.onBackPressed();
		}
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		web.saveState(outState);
		}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		//do nothing
		super.onConfigurationChanged(newConfig);
	}
}
