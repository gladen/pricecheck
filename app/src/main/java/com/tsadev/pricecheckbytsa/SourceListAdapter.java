package com.tsadev.pricecheckbytsa;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class SourceListAdapter extends BaseAdapter {
	
	Activity context;
	List<Source> values;
	
	public SourceListAdapter (Activity context, List<Source> values) {
	    this.context = context;
	    this.values = values;
	}

	@Override
	public int getCount() {
		return values.size();
	}

	@Override
	public Object getItem(int position) {
		return values.get(position);
	}

	@Override
	public long getItemId(int position) {
		return values.indexOf(values.get(position));
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
	    // reuse views
	    if (convertView == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			convertView = inflater.inflate(R.layout.source_layout, null);
			// configure view holder
			viewHolder = new ViewHolder();
			viewHolder.name = (TextView) convertView.findViewById(R.id.source_name);
			viewHolder.url = (TextView) convertView.findViewById(R.id.source_url);
			viewHolder.img = (ImageView) convertView.findViewById(R.id.source_icon);
			convertView.setTag(viewHolder);
	    }
		else viewHolder = (ViewHolder) convertView.getTag();

	    // fill data
	    Source source = values.get(position);
		String name = source.getName();
		String url = source.getUrl();
		viewHolder.name.setText(name);
		String[] urlPart1 = url.split("http://");
		String[] urlPart2 = urlPart1[1].split(".com");
		String finalUrl = urlPart2[0] + ".com";
		Downloader down = new Downloader();
		down.download(finalUrl, viewHolder.img);
		if (finalUrl.contains("google")) finalUrl += "/shopping";
		viewHolder.url.setText(finalUrl);




	    return convertView;
	  }
	
	  static class ViewHolder {
		  public TextView name,url;
		  public ImageView img;
		  }
}
