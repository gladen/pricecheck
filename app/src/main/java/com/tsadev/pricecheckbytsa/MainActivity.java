package com.tsadev.pricecheckbytsa;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.SearchView.OnSuggestionListener;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	private static final int EDIT = 80808;
	private static final int INFO = 10101;
	private static final int SEARCH = 1230;
	private SearchView searchV;

	public static String g;
	public static Context mContext;
		
	@Override
	public void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.main_layout);
		super.onCreate(savedInstanceState);
		g = "";
		mContext = this;
		//getActionBar().setIcon(android.R.drawable.ic_menu_search);

	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		searchV = new SearchView(this);
		MenuItem search = menu.add(Menu.NONE,SEARCH,0,"Search");
		search.setIcon(R.mipmap.ic_action_search).setActionView(searchV).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		MenuItem set = menu.add(Menu.NONE,EDIT,1,"Clear recent searches");
		set.setIcon(R.mipmap.ic_action_settings).setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
		MenuItem about = menu.add(Menu.NONE,INFO,2,"About");
		about.setIcon(R.mipmap.ic_action_about).setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
		
	    SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
	    searchV.setSubmitButtonEnabled(true);
	    searchV.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
	    searchV.setIconifiedByDefault(true);
	    searchV.setOnSuggestionListener(new OnSuggestionListener() {
			
			@Override
			public boolean onSuggestionSelect(int position) {
				return false;
			}
			
			@Override
			public boolean onSuggestionClick(int position) {
				g = searchV.getSuggestionsAdapter().getCursor().getString(2);
				SearchRecentSuggestions suggestions = new SearchRecentSuggestions(getBaseContext(),
		                RecentQueriesProvider.AUTHORITY, RecentQueriesProvider.MODE);
		        suggestions.saveRecentQuery(g, null);
				Intent i = new Intent(getBaseContext(), SourceActivity.class);
				startActivity(i);
				//searchV.setQuery(g, false);
				searchV.setIconified(true);
				return true;
			}
		});
	    
	    searchV.setOnQueryTextListener(new OnQueryTextListener() {
			
			@Override
			public boolean onQueryTextSubmit(String query) {
				SearchRecentSuggestions suggestions = new SearchRecentSuggestions(getBaseContext(),
	                RecentQueriesProvider.AUTHORITY, RecentQueriesProvider.MODE);
	        suggestions.saveRecentQuery(query, null);
				g = query;
				Intent i = new Intent(getBaseContext(), SourceActivity.class);
				startActivity(i);
				searchV.setQuery("", false);
				searchV.setIconified(true);
				return true;
			}

			@Override
			public boolean onQueryTextChange(String newText) {
				return false;
			}
		});
	    if (getIntent().getAction().equals(Intent.ACTION_SEARCH)) {
	    	searchV.setIconified(false);
	    	searchV.requestFocus();
	    }
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    
	    	case EDIT:
	    		//Intent i = new Intent(this, SettingsActivity.class);
	    		//startActivity(i);
				SearchRecentSuggestions suggestions = new SearchRecentSuggestions(getBaseContext(), RecentQueriesProvider.AUTHORITY, RecentQueriesProvider.MODE);
				suggestions.clearHistory();
				Toast.makeText(getApplicationContext(), "History cleared", Toast.LENGTH_SHORT).show();
	    		return true;
	    	
	    	case INFO:
				new AlertDialog.Builder(MainActivity.mContext)
				.setTitle("About")
				.setMessage("Developed by TS-A Dev.\n \u00A9 2015 Twisted Spark (Android Dev Team)")
				.setCancelable(true)
				.setNeutralButton("Done", null)
				.show();
	    		return true;
	    	
	    	default:
	    		return super.onOptionsItemSelected(item);
	    	}
		}
	
	@Override
	public void onResume() {
		super.onResume();
		int result = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (result == (ConnectionResult.SERVICE_MISSING|ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED|
				ConnectionResult.SERVICE_DISABLED)) {
			
			Dialog update = GooglePlayServicesUtil.getErrorDialog(result, this, 666);
			update.show();
			
		}
	}
	
	@Override
	public void onBackPressed() {
		if (!searchV.isIconified()) {
			searchV.setIconified(true);
		}
		else super.onBackPressed();
	}
}
