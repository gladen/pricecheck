package com.tsadev.pricecheckbytsa;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.provider.SearchRecentSuggestions;
import android.view.MenuItem;
import android.widget.Toast;

public class SettingsActivity extends PreferenceActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//getActionBar().setHomeButtonEnabled(true);
		//getActionBar().setTitle("Price Check");
		//getActionBar().setIcon(android.R.drawable.ic_menu_revert);
		PreferenceScreen root = getPreferenceManager().createPreferenceScreen(this);
		PreferenceCategory head = new PreferenceCategory(this);
		head.setTitle("Settings");
		root.addPreference(head);
		Preference clearSuggestions = new Preference(this);
		clearSuggestions.setTitle("Clear search history");
		clearSuggestions.setSummary("Clears your recent searches");
		clearSuggestions.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

			@Override
			public boolean onPreferenceClick(Preference preference) {
				SearchRecentSuggestions suggestions = new SearchRecentSuggestions(getApplicationContext(),
						RecentQueriesProvider.AUTHORITY, RecentQueriesProvider.MODE);
				suggestions.clearHistory();
				Toast.makeText(getApplicationContext(), "History cleared", Toast.LENGTH_SHORT).show();
				return true;
			}
		});
		head.addPreference(clearSuggestions);
		setPreferenceScreen(root);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    case android.R.id.home:
	    	this.finish();
	    	return true;
	    default:
	    	return super.onOptionsItemSelected(item);
	    }
	}
	

}
