package com.tsadev.pricecheckbytsa;

import android.util.Log;

public class LogUtils {
	
	private static final String tag = "TSADEV";
	
	public static void d(String string) {
		Log.d(tag, string);
	}

}
