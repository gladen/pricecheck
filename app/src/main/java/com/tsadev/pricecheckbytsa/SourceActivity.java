package com.tsadev.pricecheckbytsa;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class SourceActivity extends Activity {
	static final int SETTINGS = 34232;
	List<Source> sources = new ArrayList<>();

	String urls[] = {
			"http://amazon.com/s/?field-keywords=", "http://www.ebay.com/sch/i.html?&_nkw=",
			"http://www.google.com/#tbm=shop&q=", "http://www.walmart.com/search/search-ng.do?search_query=",
			"http://www.target.com/s?searchTerm=", "http://www.newegg.com/Product/ProductList.aspx?Submit=ENE&Description=",
			"http://www.toysrus.com/search/index.jsp?kwCatId=&kw=", "http://www.zappos.com/",
			"http://www.overstock.com/search?keywords="
	};
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initSources();
		setContentView(R.layout.source);
		getActionBar().setTitle("Search for " + "\"" + MainActivity.g + "\"");
		getActionBar().setIcon(android.R.color.transparent);
		ListView listView = (ListView)findViewById(R.id.source_list);
		listView.setAdapter(new SourceListAdapter(this, sources));

		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Source clicked = sources.get(position);
				startActivity(new Intent(getApplicationContext(), WebActivity.class)
						.putExtra("url", clicked.getUrl())
				);
			}
		});
		AdView advert = (AdView)findViewById(R.id.advert);
		AdRequest adRequest = new AdRequest.Builder().addTestDevice("0ED3C6515C28A742E95BB310576BF558").build();
		advert.loadAd(adRequest);
	}

	private void initSources() {
		sources.add(new Source("Amazon", "http://www.amazon.com/s/?field-keywords="));
		sources.add(new Source("eBay", "http://m.ebay.com/sch/i.html?_nkw="));
		//Fuck Google sources.add(new Source("Google Shopping", "http://www.google.com/#tbm=shop&q="));
		sources.add(new Source("Walmart", "http://www.walmart.com/search/?query="));
		sources.add(new Source("Target", "http://m.target.com/s?searchTerm="));
		sources.add(new Source("Newegg", "http://m.newegg.com/ProductList?keyword="));
		sources.add(new Source("Toys R Us", "http://www.toysrus.com/search/index.jsp?kwCatId=&kw="));
		sources.add(new Source("Zappos", "http://m.zappos.com/"));
		sources.add(new Source("Overstock", "http://www.overstock.com/search?keywords="));
	}
	
	
	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//MenuItem set = menu.add(Menu.NONE,SETTINGS,Menu.NONE,"Settings");
		//set.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		//set.setIcon(android.R.drawable.ic_menu_edit);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    
	    case android.R.id.home:
	    	this.finish();
	    	return true;
	    	
	    case SETTINGS:
	    	startActivity(new Intent(this, SettingsActivity.class));
	    	return true;
	    	
	    default:
	    	return super.onOptionsItemSelected(item);
	    }
	}
	
	
}
